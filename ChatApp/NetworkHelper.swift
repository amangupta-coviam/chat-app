//
//  NetworkHelper.swift
//  ChatApp
//
//  Created by Aman Gupta on 25/07/19.
//  Copyright © 2019 Aman Gupta. All rights reserved.
//

import Foundation
import FirebaseAuth
import FirebaseFirestore

class NetworkHelper{
    var db = Firestore.firestore()
    
    let USER_ID = "userId"
    let USER_NAME = "name"
    let USERS = "users"
    let USERNAME = "userName"
    let GROUPID = "groupId"
    let GROUPNAME = "groupName"
    let GROUPS = "groups"
    let MESSAGE = "message"
    let SENDER_ID = "senderId"
    let SENDER_NAME = "senderName"
    
    func fetchAllUsersList(completion:@escaping (_:[UserDataModel])->Void){
        var dataArray = Set<UserDataModel>()
        db.collection(self.USERS).getDocuments { (data, error) in
            guard let dataRecieved = data , error == nil else{
                print(error!)
                return
            }
            for document in dataRecieved.documents{
                let d = document.data()
                if let name = d[self.USER_NAME] as? String,let userId = d[self.USER_ID] as? String,userId != Auth.auth().currentUser?.uid {
                    let user = UserDataModel(name: name, id: userId)
                    dataArray.insert(user)
                }
            }
            completion(Array(dataArray))
        }
    }
    
    func fetchPersonalChatUsersList(completion:@escaping (_:Set<HomePageDataModel> ) -> Void ){
        var userData = Set<HomePageDataModel>()
        if let currentUserId = Auth.auth().currentUser?.uid{
            
            db.collection("users/\(currentUserId)/chats").document("personalChats").addSnapshotListener { (documentSnapshot, error) in
                guard let documentFetched = documentSnapshot else{
                    print(error!)
                    return
                }
                guard let data = documentFetched.data(), let dataArray = data[self.USERS] as? [[String:Any]] else{
                    if let error = error{
                        print(error)
                    }
                    completion(userData)
                    return
                }
                for d in dataArray{
                    if let id = d[self.USER_ID] as? String,let name = d[self.USERNAME] as? String{
                        let user = HomePageDataModel(name: name, id: id)
                        userData.insert(user)
                    }
                    
                }
                completion(userData)
            }
        }
    }
    
    func fetchGroupChatList(completion:@escaping (_:Set<HomePageDataModel> ) -> Void ){
        var groupData = Set<HomePageDataModel>()
        if let currentUserId = Auth.auth().currentUser?.uid{
            
            db.collection("users/\(currentUserId)/chats").document("groupChats").addSnapshotListener { (documentSnapshot, error) in
                guard let documentFetched = documentSnapshot else{
                    return
                }
                guard let dataRecieved = documentFetched.data() , let groupsArray=dataRecieved[self.GROUPS] as? [[String:Any]] else{
                    if let error = error{
                        print(error)
                    }
                    completion(groupData)
                    return
                }
                for d in groupsArray{
                    if let groupId = d[self.GROUPID] as? String, let groupName = d[self.GROUPNAME] as? String{
                        let group = HomePageDataModel(name: groupName, id: groupId)
                        group.isGroup = true
                        groupData.insert(group)
                    }
                }
                completion(groupData)
            }
        }
    }
    
    
    func startNewChatWith(userName:String,userId:String , completion:()->Void){
        if let currentUserId = Auth.auth().currentUser?.uid , let displayName = Auth.auth().currentUser?.displayName{
            var dataToBeSent : [[String:String]] = [[self.USERNAME:userName,self.USER_ID:userId]]
            db.collection("users/\(currentUserId)/chats").document("personalChats").setData ( [self.USERS : FieldValue.arrayUnion(dataToBeSent) ] , merge: true) { (error) in
                if let error = error{
                    print(error)
                }
            }
            dataToBeSent = [[self.USERNAME:displayName,self.USER_ID:currentUserId]]
            db.collection("users/\(userId)/chats").document("personalChats").setData ( [self.USERS : FieldValue.arrayUnion(dataToBeSent) ] , merge: true) { (error) in
                if let error = error{
                    print(error)
                }
            }
            completion()
            
        }
    }
    
    func createNewGroupWith(name:String,users:[UserDataModel] , completion:@escaping (_ name :String , _ groupId :String)->Void ){
        if let currentUser = Auth.auth().currentUser , let currentUserDisplayName = currentUser.displayName{
            var dataToBeSent : [[String:String]] = []
            for user in users{
                let dict = [self.USERNAME:user.userName,self.USER_ID:user.userId]
                dataToBeSent.append(dict)
            }
            dataToBeSent.append([self.USERNAME:currentUserDisplayName,self.USER_ID:currentUser.uid])
            var ref : DocumentReference? = nil
            ref = db.collection(self.GROUPS).addDocument(data: [self.USERS:FieldValue.arrayUnion(dataToBeSent),self.GROUPNAME:name]) { (error) in
                if let error = error{
                    print(error)
                }
                else if let groupId = ref?.documentID {
                    completion(name,groupId)
                    let groupDetails :[[String:String]] = [[self.GROUPNAME:name,self.GROUPID:groupId]]
                    for data in dataToBeSent{
                        if let userId = data[self.USER_ID]{
                        self.db.collection("users/\(userId)/chats").document("groupChats").setData([self.GROUPS:FieldValue.arrayUnion(groupDetails)],merge: true){
                                (error) in
                                if let error = error{
                                    print(error)
                                }
                            }
                        }
                    }
                    self.db.collection("chats").document("groupChats").setData([groupId:[]],merge: true, completion: { (error) in
                        if let error = error{
                            print(error)
                        }
                    })
                    
            }
        }
       
        }
    }
    
    //MARK: Fetching and sending messages
    
    func sendMessage(message:ChatMessageDataModel ,chatId:String, isGroupChat:Bool){
        guard let currentUserId = Auth.auth().currentUser?.uid, let displayName = Auth.auth().currentUser?.displayName else {
            return
        }
        message.senderId = currentUserId
        message.senderName = displayName
        let messageBody : [[String:String]] = [[self.MESSAGE:message.messageText,self.SENDER_ID:message.senderId,self.SENDER_NAME:message.senderName]]
        var chatType = ""
        var id = chatId
        if isGroupChat{
            chatType = "groupChats"
        }
        else{
            chatType = "personalChat"
            id = getChatIdFor(currentUser: currentUserId, newChatUser: chatId)
        }
        
        db.collection("chats").document(chatType).setData([id:FieldValue.arrayUnion(messageBody)], merge: true) { (error) in
            if let error = error{
                print(error)
            }
        }
        
    }
    
    func fetchMessages(for id:String,chatType type:String,currentUserId :String, completion  :@escaping (_:[ChatMessageDataModel]) -> Void ){
        
        var chatMessages = [ChatMessageDataModel]()
        
        db.collection("chats").document(type).addSnapshotListener { (documentSnapshot, error) in
            guard let document = documentSnapshot else{
                print(error!)
                return
            }
            guard let data = document.data(),let chats = data[id] as? [[String:Any]] else {
                if let error = error{
                    print(error)
                }
                return
            }
            chatMessages = []
            for chat in chats{
                if let message = chat[self.MESSAGE] as? String, let senderId = chat[self.SENDER_ID] as? String, let senderName = chat[self.SENDER_NAME] as? String{
                    let chatMessage = ChatMessageDataModel(messageText: message, senderName: senderName, senderId: senderId)
                    chatMessage.isReceived = senderId == currentUserId ? false : true
                    chatMessages.append(chatMessage)
                }
            }
            completion(chatMessages)
        }
    }
    
    
    func getChatMessages(for id:String,isGroupChat:Bool, completion : @escaping (_:[ChatMessageDataModel] )->Void ){
        var chatMessages = [ChatMessageDataModel]()
        if let currentUserId = Auth.auth().currentUser?.uid{
            if !isGroupChat{
                let chatId = getChatIdFor(currentUser: currentUserId, newChatUser: id)
                fetchMessages(for: chatId, chatType: "personalChat", currentUserId: currentUserId ){ (data) in
                    chatMessages = []
                    chatMessages = data
                    completion(chatMessages)
                }
            }
            else if isGroupChat{
                fetchMessages(for: id, chatType: "groupChats", currentUserId: currentUserId ){ (data) in
                    chatMessages = data
                    completion(chatMessages)
                }
            }
        }
        
    }
    
    //MARK: helper function
    
    func getChatIdFor(currentUser:String,newChatUser:String) -> String{
        if currentUser<newChatUser{
            return currentUser+newChatUser
        }
        else {
            return newChatUser+currentUser
        }
    }
    func createInstanceInFirestore()
    {
        if let userId = Auth.auth().currentUser?.uid,let userName = Auth.auth().currentUser?.displayName{
            let storePath = db.collection(self.USERS).document(userId)
            storePath.setData([self.USER_ID:userId,self.USER_NAME:userName]) { (error) in
                if let error = error{
                    print(error)
                }
            }
        }
    }
    
    
    //MARK:Authentication Functions
    func registerNewUser(_ email: String, _ password: String, _ name: String,completion:@escaping (_ error:(String,String)?)->Void) {
        Auth.auth().createUser(withEmail: email, password: password) { (userAuth, error) in
            guard let user = userAuth?.user , error == nil else{
                if let error = error{
                    completion(("Error",error.localizedDescription))
                }
                return
                
            }
            UserDefaults.standard.set(user.uid, forKey: self.USER_ID)
            let profileChangeRequest = user.createProfileChangeRequest()
            profileChangeRequest.displayName = name
            profileChangeRequest.commitChanges(completion: { (error) in
                if let error = error{
                    completion(("Error",error.localizedDescription))
                }
                else{
                    self.createInstanceInFirestore()
                    completion(nil)
                    
                }
            })
        }
    }
    func loginUser(_ email: String, _ password: String, completion:@escaping(_ error:(String,String)?)->Void) {
        Auth.auth().signIn(withEmail: email, password: password) { (authResult, error) in
            if let auth = authResult?.user , error == nil {
                UserDefaults.standard.set(auth.uid, forKey: self.USER_ID)
                completion(nil)
            }
            else {
                completion(("Invalid Credentials","Make sure you enter correct email and password"))
            }
        }
    }
    
    @objc func logoutButtonPressed(completion:()->Void){
        do{
            try Auth.auth().signOut()
            completion()
        }
        catch{
            print("error Occured")
        }
    }
}

