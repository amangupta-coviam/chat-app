//
//  ChatMessageDataModel.swift
//  ChatApp
//
//  Created by Aman Gupta on 26/07/19.
//  Copyright © 2019 Aman Gupta. All rights reserved.
//

import Foundation

class ChatMessageDataModel {
    var messageText = ""
    var senderName = ""
    var senderId = ""
    var isReceived = false
    
    init(messageText:String){
        self.messageText = messageText
    }
    
    init(messageText:String,senderName:String,senderId:String,isReceived:Bool){
        self.messageText = messageText
        self.senderName = senderName
        self.isReceived = isReceived
        self.senderId = senderId
    }
    init(messageText:String,senderName:String,senderId:String){
        self.messageText = messageText
        self.senderName = senderName
        self.senderId = senderId
    }
}
