//
//  File.swift
//  ChatApp
//
//  Created by Aman Gupta on 26/07/19.
//  Copyright © 2019 Aman Gupta. All rights reserved.
//

import Foundation

class HomePageDataModel:Equatable,Hashable{
    
    
    var name = ""
    var id = ""
    var isGroup = false
    
    init(name:String,id:String) {
        self.id = id
        self.name = name
    }
    init(){
        
    }
    static func == (lhs: HomePageDataModel, rhs: HomePageDataModel) -> Bool {
        return lhs.id == rhs.id && lhs.name == rhs.name
    }
    func hash(into hasher: inout Hasher) {
        hasher.combine(name)
        hasher.combine(id)
        hasher.combine(isGroup)
    }
}
