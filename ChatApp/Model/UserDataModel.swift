//
//  UserDataModel.swift
//  ChatApp
//
//  Created by Aman Gupta on 25/07/19.
//  Copyright © 2019 Aman Gupta. All rights reserved.
//

import Foundation
class UserDataModel : Equatable,Hashable{
    var userName = ""
    var userId = ""
    var isSelected = false
    func hash(into hasher: inout Hasher) {
        hasher.combine(userName)
        hasher.combine(userId)
    }
    static func == (lsh:UserDataModel,rhs:UserDataModel)->Bool{
        return lsh.userId==rhs.userId || lsh.userName == rhs.userName
    }
    init(name:String,id:String){
        self.userName = name
        self.userId = id
    }
    
    
}

