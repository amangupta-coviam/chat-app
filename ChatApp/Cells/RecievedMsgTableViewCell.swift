//
//  RecievedMsgTableViewCell.swift
//  ChatApp
//
//  Created by Aman Gupta on 26/07/19.
//  Copyright © 2019 Aman Gupta. All rights reserved.
//

import UIKit

class RecievedMsgTableViewCell: UITableViewCell {

    @IBOutlet weak var msg: UILabel!
    @IBOutlet weak var name: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
