//
//  AppDelegate.swift
//  ChatApp
//
//  Created by Aman Gupta on 24/07/19.
//  Copyright © 2019 Aman Gupta. All rights reserved.
//

import UIKit
import Firebase
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        
        return true
    }
}
extension UIViewController{
    
    func showToast(title:String,message:String){
        if let toastView = view.viewWithTag(101){
            toastView.removeFromSuperview()
        }
        let newToastView = UIView()
        newToastView.translatesAutoresizingMaskIntoConstraints = false
        newToastView.layer.cornerRadius = 10
        newToastView.tag = 101
        view.addSubview(newToastView)
        newToastView.bringSubviewToFront(view)
        newToastView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: -30).isActive = true
        newToastView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        newToastView.heightAnchor.constraint(equalToConstant: 70).isActive = true
        newToastView.widthAnchor.constraint(equalToConstant: self.view.frame.width/2).isActive = true
        newToastView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        let label = UILabel()
        label.numberOfLines = 4
        label.font = UIFont(name: label.font.fontName, size: 12)
        label.text = "\(title)\n\(message)"
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        newToastView.addSubview(label)
        label.centerYAnchor.constraint(equalTo: newToastView.centerYAnchor).isActive = true
        label.centerXAnchor.constraint(equalTo: newToastView.centerXAnchor).isActive = true
        label.heightAnchor.constraint(equalToConstant: 50).isActive = true
        label.widthAnchor.constraint(equalTo: newToastView.widthAnchor, multiplier: 1).isActive = true
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()+2) {
            newToastView.removeFromSuperview()
        }
        
    }
    func applyImageToBackground(){
        
        UIGraphicsBeginImageContext(self.view.frame.size)
        UIImage(named: "image")?.draw(in: self.view.bounds)
        if let image = UIGraphicsGetImageFromCurrentImageContext(){
            UIGraphicsEndImageContext()
            self.view.backgroundColor = UIColor(patternImage: image)
            self.view.layer.contents = image.cgImage
        }
        else {
            UIGraphicsEndImageContext()
            self.view.backgroundColor = UIColor.blue
        }
    }
    
}
extension UIView{
    func startActivityIndicator(){
        
        let view = UIView()
        view.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        view.center = self.center
        view.backgroundColor = #colorLiteral(red: 0.2666666667, green: 0.2666666667, blue: 0.2666666667, alpha: 0.7)
        view.clipsToBounds = true
        view.tag = 110
        view.layer.cornerRadius = 10
        
        let progressIndicator = UIActivityIndicatorView()
        progressIndicator.frame = CGRect(x: 0, y: 0, width: 30, height: 40)
        progressIndicator.center = CGPoint(x: view.frame.width/2, y: view.frame.height/2)
        progressIndicator.style = .whiteLarge
        
        view.addSubview(progressIndicator)
        self.addSubview(view)
        UIApplication.shared.beginIgnoringInteractionEvents()
        progressIndicator.startAnimating()
    }
    func stopActivityIndicator()
    {
        if let progress = self.viewWithTag(110){
            UIApplication.shared.endIgnoringInteractionEvents()
            progress.removeFromSuperview()
        }
    }
}


