//
//  NewGroupViewController.swift
//  ChatApp
//
//  Created by Aman Gupta on 25/07/19.
//  Copyright © 2019 Aman Gupta. All rights reserved.
//

import UIKit


class NewGroupViewController: UIViewController {
    
    var userList = [UserDataModel]()
    var selectedUsersForGroup = Set<UserDataModel>()
    var networkHelper = NetworkHelper()
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var groupNameTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        applyImageToBackground()
        
        view.startActivityIndicator()
        networkHelper.fetchAllUsersList(completion: { (userList) in
            self.userList = userList
            self.view.stopActivityIndicator()
            self.tableView.reloadData()
        })
        tableView.backgroundColor = UIColor.clear.withAlphaComponent(0)
    }
    

    @IBAction func createGroupButtonPressed(_ sender: UIButton) {
        if let groupName = groupNameTextField.text{
            if groupName.isEmpty{
                groupNameTextField.becomeFirstResponder()
                
                showToast(title: "No Group Name!!", message: "Please Enter a group name to continue...")
                return
            }
            else if selectedUsersForGroup.count > 0 {
                networkHelper.createNewGroupWith(name: groupName, users: Array(selectedUsersForGroup)) { (_, id) in
                    HomeViewController.moveToChatFromHome(groupName, id, true)
                    self.navigationController?.popViewController(animated: true)
                }
            }
            else{
                showToast(title: "No Users Selected", message: "Please select atleast 1 user to continue...")
            }
        }
    }
}
extension NewGroupViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = userList[indexPath.row].userName
        cell.selectionStyle = .none
        cell.backgroundColor = UIColor.clear.withAlphaComponent(0)
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        userList[indexPath.row].isSelected = !userList[indexPath.row].isSelected
        if !userList[indexPath.row].isSelected{
            tableView.cellForRow(at: indexPath)?.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            selectedUsersForGroup.remove(userList[indexPath.row])
        }
        else{
            tableView.cellForRow(at: indexPath)?.backgroundColor = #colorLiteral(red: 0.6869916524, green: 0.8122056935, blue: 1, alpha: 1)
            selectedUsersForGroup.insert(userList[indexPath.row])
        }
    }
    
    
}
