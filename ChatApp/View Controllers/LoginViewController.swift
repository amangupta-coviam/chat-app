//
//  LoginViewController.swift
//  ChatApp
//
//  Created by Aman Gupta on 25/07/19.
//  Copyright © 2019 Aman Gupta. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    
    var networkHelper = NetworkHelper()
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        applyImageToBackground()
    }
    
    
    @IBAction func loginButtonPressed(_ sender: UIButton) {
        
        guard let email = emailTextField.text, let password = passwordTextField.text else{
            return
        }
        if email.isEmpty {
            emailTextField.becomeFirstResponder()
        }
        else if password.isEmpty{
            passwordTextField.becomeFirstResponder()
        }
        else{
            self.view.startActivityIndicator()
            networkHelper.loginUser(email, password) { (error) in
                self.view.stopActivityIndicator()
                if let error = error{
                    self.showToast(title: error.0, message: error.1)
                }
                else{
                    
                    HomeViewController.moveToHomeVC()
                }
            }
        }
        
    }
    static func moveToLoginVC()
    {
        if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "loginViewControllerID") as? LoginViewController {
            
            let nav = UINavigationController(rootViewController: vc)
            UIApplication.shared.keyWindow?.rootViewController = nav
        }
    }

}
