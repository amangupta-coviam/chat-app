//
//  HomeViewController.swift
//  ChatApp
//
//  Created by Aman Gupta on 25/07/19.
//  Copyright © 2019 Aman Gupta. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var newPersonalChatButton: UIButton!
    @IBOutlet weak var newGroupButton: UIButton!
    
    var chatList = Set<HomePageDataModel>()
    var chatListArray = [HomePageDataModel]()
    var listLoadedCounter = 0
    var networkHelper = NetworkHelper()
    
    static var dataForChatScreen = HomePageDataModel()
    static var redirectToChatScreen : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        fetchList()
        applyImageToBackground()
        tableView.separatorStyle = .none
        tableView.backgroundColor = UIColor.clear.withAlphaComponent(0)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Logout", style: .plain, target: self, action: #selector
            (logoutButtonPressed) )
        initValues()
        if HomeViewController.redirectToChatScreen{
            shouldRedirect()
        }
    }
    fileprivate func initValues() {
        chatList = []
        listLoadedCounter = 0
    }
    static func moveToHomeVC(){
        if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "homeViewControllerId") as? HomeViewController {
            let nav = UINavigationController(rootViewController: vc)
            UIApplication.shared.keyWindow?.rootViewController = nav
        }
    }
    @objc func logoutButtonPressed(){
        networkHelper.logoutButtonPressed {
            UserDefaults.standard.removeObject(forKey: "userId")
            LoginViewController.moveToLoginVC()
            
        }
        
    }
    func checkIfBothListsRecieved(){
        listLoadedCounter += 1
        if listLoadedCounter == 2{
            self.view.stopActivityIndicator()
        }
        tableView.reloadData()
    }
    fileprivate func fetchList() {
        self.view.startActivityIndicator()
        networkHelper.fetchPersonalChatUsersList { (userList) in
            for user in userList{
                self.chatList.insert(user)
            }
            self.checkIfBothListsRecieved()
            self.chatListArray = Array(self.chatList)
        }
        
        networkHelper.fetchGroupChatList { (groupList) in
            for group in groupList{
                self.chatList.insert(group)
            }
            self.checkIfBothListsRecieved()
            self.chatListArray = Array(self.chatList)
        }
    }
    func shouldRedirect() {
        HomeViewController.redirectToChatScreen = false
        if let vc = ChatViewController.getChatViewController(){
            vc.chatId = HomeViewController.dataForChatScreen.id
            vc.name = HomeViewController.dataForChatScreen.name
            vc.isGroupChat = HomeViewController.dataForChatScreen.isGroup
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    static func moveToChatFromHome(_ name: String, _ chatId: String, _ isGroupChat: Bool) {
        dataForChatScreen.name = name
        dataForChatScreen.id = chatId
        dataForChatScreen.isGroup = isGroupChat
        redirectToChatScreen = true
    }
}
extension HomeViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = chatListArray[indexPath.row].name
        cell.selectionStyle = .none
        cell.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 0.1998876284)
        
        let border = CALayer()
        border.borderColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        border.frame = CGRect(x: 0, y: cell.frame.height - 2, width: self.tableView.frame.width, height: self.tableView.frame.height)
        border.borderWidth = 1
        cell.layer.addSublayer(border)
        cell.layer.masksToBounds = true
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let name = chatListArray[indexPath.row].name
        let chatId = chatListArray[indexPath.row].id
        let isGroupChat = chatListArray[indexPath.row].isGroup
        HomeViewController.moveToChatFromHome(name, chatId, isGroupChat)
        shouldRedirect()
    }
    
}
