//
//  ChatViewController.swift
//  ChatApp
//
//  Created by Aman Gupta on 26/07/19.
//  Copyright © 2019 Aman Gupta. All rights reserved.
//

import UIKit

class ChatViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var messageTextViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var messageTextField: UITextField!
    
    var networkHelper = NetworkHelper()
    var chatMessages = [ChatMessageDataModel]()
    var name = ""
    var chatId = ""
    var isGroupChat = false
    
    fileprivate func updateTableView() {
        self.tableView.reloadData()
        scrollToBottom()
    }
    func scrollToBottom() {
        DispatchQueue.main.async {
            let numberOfSections = self.tableView.numberOfSections
            let indexPath = IndexPath(row: 0,section: numberOfSections-1)
            self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        registerKeyboardNotifications()
        navigationItem.title = name
        applyImageToBackground()
        tableView.backgroundColor = UIColor.clear.withAlphaComponent(0)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 11
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        networkHelper.getChatMessages(for: chatId, isGroupChat: isGroupChat) { (messagesRecieved) in
            self.chatMessages = messagesRecieved
            self.updateTableView()
        }
    }
    
    @IBAction func sendButtonPressed(_ sender: UIButton) {
        if let messageText = messageTextField.text{
            let message = ChatMessageDataModel(messageText: messageText)
            networkHelper.sendMessage(message: message, chatId: chatId, isGroupChat: isGroupChat)
            chatMessages.append(message)
            messageTextField.text = ""
        }
    }
    
    static func getChatViewController()->ChatViewController?{
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "chatStoryboard") as? ChatViewController
    }
    
    func registerKeyboardNotifications() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(
            self,
            selector: #selector(keyboardWillShow(notif:)),
            name: UIResponder.keyboardWillShowNotification,
            object: nil)
        notificationCenter.addObserver(
            self,
            selector: #selector(keyboardWillShow(notif:)),
            name: UIResponder.keyboardWillChangeFrameNotification,
            object: nil)
        notificationCenter.addObserver(
            self,
            selector: #selector(keyboardWillHide(notif:)),
            name: UIResponder.keyboardWillHideNotification,
            object: nil)
    }
    
    @objc func keyboardWillShow(notif:Notification){
        guard let frame = notif.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect else{
            return
        }
        self.messageTextViewBottomConstraint.constant = frame.height
    }
    @objc func keyboardWillHide(notif:Notification){
        self.messageTextViewBottomConstraint.constant = 10
    }

}
extension ChatViewController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return chatMessages.count
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 20
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = UIColor.clear.withAlphaComponent(0)
        return view
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let chatMessage = chatMessages[indexPath.section]
        let cell = UITableViewCell()
        cell.layer.borderWidth = 2
        cell.layer.borderColor = #colorLiteral(red: 0.1215686275, green: 0.6509803922, blue: 0.6274509804, alpha: 1)
        cell.layer.cornerRadius = 10
        cell.backgroundColor = UIColor.clear.withAlphaComponent(0)
        cell.textLabel?.numberOfLines = 0
        if chatMessage.isReceived{
            let senderName = chatMessage.senderName
            let message = chatMessage.messageText
            let string1 = NSMutableAttributedString(attributedString: NSAttributedString(string: senderName, attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 11)] ) )
            let string2 = NSAttributedString(string: "\n\(message)", attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 17)])
            string1.append(string2)
            cell.textLabel?.attributedText = string1
        }
        else{
            cell.textLabel?.attributedText = NSAttributedString(string: chatMessage.messageText, attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 17)])
            cell.textLabel?.textAlignment = .right
        }
        return cell
    }
    
}
