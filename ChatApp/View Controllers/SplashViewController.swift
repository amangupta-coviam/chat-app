//
//  SplashViewController.swift
//  ChatApp
//
//  Created by Aman Gupta on 25/07/19.
//  Copyright © 2019 Aman Gupta. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = #colorLiteral(red: 1, green: 0.9215686275, blue: 0.5215686275, alpha: 1)
        applyImageToBackground()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if let data = UserDefaults.standard.value(forKey: "userId"){
            print("User already Logged in \(data)")
            HomeViewController.moveToHomeVC()

        }
        else{
            LoginViewController.moveToLoginVC()
        }

    }

}
