//
//  RegisterViewController.swift
//  ChatApp
//
//  Created by Aman Gupta on 24/07/19.
//  Copyright © 2019 Aman Gupta. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {

    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    var networkHelper = NetworkHelper()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        applyImageToBackground()
        
        initValues()
        // Do any additional setup after loading the view.
    }
    func isValidEmail(emailStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: emailStr)
    }
    fileprivate func initValues() {
        nameTextField.tag = 1001
        nameTextField.delegate = self
        emailTextField.tag = 1002
        emailTextField.delegate = self
        passwordTextField.tag = 1003
        passwordTextField.delegate = self
        confirmPasswordTextField.tag = 1004
        confirmPasswordTextField.delegate = self
    }
    
    
    
    @IBAction func registerButtonClicked(_ sender: UIButton) {
        
        if let name = nameTextField.text,let email = emailTextField.text,let password = passwordTextField.text,let confirmPassword = confirmPasswordTextField.text{
            if name.isEmpty{
                nameTextField.becomeFirstResponder()
                showToast(title: "Enter Name", message: "")
            }
            else if email.isEmpty{
                emailTextField.becomeFirstResponder()
            }
            else if !isValidEmail(emailStr: email){
                showToast(title: "Invalid Email", message: "Make sure you enter correct email address")
                emailTextField.becomeFirstResponder()
            }
            else if password.isEmpty{
                passwordTextField.becomeFirstResponder()
            }
            else if confirmPassword.isEmpty{
                confirmPasswordTextField.becomeFirstResponder()
            }
            else if confirmPassword != password{
                showToast(title: "Passwords Don't Match", message: "Make sure passwords in both fields match!!")
            }
            else if password.count<6{
                showToast(title: "Password is too short", message: "Make sure the password is atleast 6 characters long")
            }
            else {
                self.view.startActivityIndicator()
                networkHelper.registerNewUser(email, password, name) { (error) in
                    self.view.stopActivityIndicator()
                    if let error = error{
                        self.showToast(title: error.0, message: error.1)
                    }
                    else{
                        HomeViewController.moveToHomeVC()
                    }
                }
                
            }
        }
        
    }
    
    static func moveToRegisterVC()
    {
        
        if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "registerViewControllerId") as? RegisterViewController {
            
            let nav = UINavigationController(rootViewController: vc)
            UIApplication.shared.keyWindow?.rootViewController = nav
        }
    }

}
extension RegisterViewController : UITextFieldDelegate{
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField.tag {
        case 1001:
            if let name = textField.text,name.isEmpty{
                showToast(title: "Enter Name", message: "")
                textField.layer.borderColor = UIColor.red.cgColor
                textField.layer.borderWidth = 2
            }
            else{
                textField.layer.borderColor = UIColor.clear.cgColor
            }
        case 1002:
            if let email = textField.text , !isValidEmail(emailStr: email){
                showToast(title: "Invalid Email", message: "Make sure you enter correct email address")
                textField.layer.borderColor = UIColor.red.cgColor
                textField.layer.borderWidth = 2
            }
            else{
                textField.layer.borderColor = UIColor.clear.cgColor
            }
        case 1003:
            if let password = textField.text , password.count<6{
                showToast(title: "Password Too Short", message: "Make sure password is atleast 6 chracter long")
                textField.layer.borderColor = UIColor.red.cgColor
                textField.layer.borderWidth = 2
            }
            else{
                textField.layer.borderColor = UIColor.clear.cgColor
            }
        case 1004:
            if let password = passwordTextField.text , let confirmPassword = textField.text{
                if password != confirmPassword{
                    showToast(title: "Password's dont match", message: "Enter same password in both fields")
                    textField.layer.borderColor = UIColor.red.cgColor
                    textField.layer.borderWidth = 2
                }
                else{
                    textField.layer.borderColor = UIColor.clear.cgColor
                }
            }
        default:
            break;
        }
    }
}
