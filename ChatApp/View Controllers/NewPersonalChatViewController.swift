//
//  NewPersonalChatViewController.swift
//  ChatApp
//
//  Created by Aman Gupta on 25/07/19.
//  Copyright © 2019 Aman Gupta. All rights reserved.
//

import UIKit

class NewPersonalChatViewController: UIViewController {

    
    @IBOutlet weak var tableView: UITableView!
    
    var data = [UserDataModel]()
    var networkHelper = NetworkHelper()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.startActivityIndicator()
        networkHelper.fetchAllUsersList { (dataReceived) in
            self.view.stopActivityIndicator()
            self.data = dataReceived
            self.tableView.reloadData()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        applyImageToBackground()
        tableView.backgroundColor = UIColor.clear.withAlphaComponent(0)
    }

}
extension NewPersonalChatViewController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = data[indexPath.row].userName
        cell.selectionStyle = .none
        cell.backgroundColor = UIColor.clear.withAlphaComponent(0)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let name = data[indexPath.row].userName
        let id = data[indexPath.row].userId
        networkHelper.startNewChatWith(userName: name, userId: id){
            self.navigationController?.popViewController(animated: true)
            HomeViewController.moveToChatFromHome(name, id, false)
        }
        
    }
    
    
    
    
}
